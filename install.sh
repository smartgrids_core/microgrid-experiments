#!/bin/bash
set -e

{ # try
    python -c 'import sys; assert sys.version_info >= (3, 7)' > /dev/null 2>&1
} || { # catch
    echo ""
    echo "Python version is incorrect (expected version: 3.7.*)"
    echo ""
    exit -1
}

./scripts/update_repositories.sh

pip install --upgrade pip
pip install packages/* black wheel setuptools j2cli markupsafe==2.0.1 pyyaml
