
# README

## Pre-requisities

### Create a virtual environment (python3.7)

```bash
python3.7 -m venv venv
source venv/bin/activate
```

### Clone repositories

Clone the following repositories:

- [dataframe_pipeline](https://bitbucket.org/smartgrids_core/dataframe_pipeline/src/master/)
- [decision_process](https://bitbucket.org/smartgrids_core/decision_process/src/master/)
- [decision_process_collection](https://bitbucket.org/smartgrids_core/decision_process_collection/src/master/)
- [forecast](https://gitlab.uliege.be/ems/forecast)
- [microgrid](https://bitbucket.org/smartgrids_core/microgrid/src/master/)
- [evkit](https://bitbucket.org/smartgrids_core/evkit/src/master/)

File structure should be as follow:

```code
dependencies/
└───dataframe_pipeline/
└───decision_process/
└───decision_process_collection/
└───evkit/
└───forecast/
└───microgrid/
```

### Install dependencies

```bash
./install.sh
```

### Reconstruct data

```bash
python scripts/reconstruct_data.py
```

### Build predictors

```bash
./scripts/build_predictors/build_predictors.sh
```

## Initialize a project

*note: a template folder should be defined in `template/`.*

```bash
# Example with project 'merygrid'
./scripts/docker_setup_update.sh merygrid

# Example with all projects
./scripts/docker_setup_update.sh \
    merygrid \
    merygrid_forecast \
    merygrid_static_evs \
    merygrid_static_evs_forecast \
    merygrid_evs \
    merygrid_evs_forecast \
    merygrid_pv \
    merygrid_pv_forecast \
    merygrid_pv_evs \
    merygrid_pv_evs_forecast \
    merygrid_pv_static_evs \
    merygrid_pv_static_evs_forecast \
    merygrid_wt \
    merygrid_wt_forecast \
    merygrid_wt_evs \
    merygrid_wt_evs_forecast \
    merygrid_wt_static_evs \
    merygrid_wt_static_evs_forecast
```

Run the following command for testing the installation locally:

```bash
# Simulation (project: merygrid)
python -m uliege.microgrid simulate \
    --microgrid ./merygrid/data/microgrid.json \
    --xlsx_microgrid_data_supplier ./merygrid/data/2020-06_2021-05.xlsx \
    --controller ./merygrid/controller/optim_forward_controller.json \
    --start_date '2020-05-31 22:00:00' \
    --periods 24 \
    --period_duration 1 \
    --period_duration_unit hour \
    --json_override \
    --control_horizon 24 \
    --reduce_control_horizon_towards_end \
    --output out_microgrid_simulation.dat
```

Expected output:

```bash
[INFO]:
[INFO]: Creating cache for data_supplier "data_supplier" [microgrid=MeryGrid, start_date=2020-05-31 22:00:00, end_date=2020-06-01 22:00:00, safe_mode=False]
...
[INFO]: Cache for data_supplier "data_supplier" has been created [microgrid=MeryGrid, start_date=2020-05-31 22:00:00, end_date=2020-06-01 22:00:00, safe_mode=False]
[INFO]:
[INFO]: Simulation starting.
[INFO]:
[WARNING]: A metering period should contain at least 1 control period.
  >> before: 1 metering period equals to 0.25 control period.
  >> now:    1 metering period equals to 1 control period.

Progress |################################| 24/24 - 0s ETA (~1s per step)
[INFO]:
[INFO]: Simulation completed in 39s.
[INFO]:
[INFO]: ============= RESULTS =============
[INFO]: [Energy Cost: -21.69€, Solution Cost: -21.687860877]
[INFO]:
[INFO]: [Member: ECM] from <2020-05-31 22:00:00> to <2020-06-01 21:00:00> (freq: <1H>)
[INFO]:   - total_energy_cost:             0.0€
[INFO]:   - energy_demand:                 0.0kWh
[INFO]:   - energy_production:             0.0kWh
[INFO]:   - storages_energy_demand:        54.1kWh
[INFO]:   - storages_energy_production:    150.15kWh
[INFO]:   - self_consumed_energy:          0.0kWh
[INFO]:   - grid_import_energy:            0.0kWh
[INFO]:   - grid_export_energy:            0.0kWh
[INFO]:   - community_import_energy:       54.1kWh
[INFO]:   - community_export_energy:       150.15kWh
[INFO]:   - grid_import_peak:              0.0kW at <2020-05-31 22:00:00> (previously: 0.0kW at <2020-05-31 22:00:00>)
[INFO]:
[INFO]: [Member: CBV] from <2020-05-31 22:00:00> to <2020-06-01 21:00:00> (freq: <1H>)
[INFO]:   - total_energy_cost:             0.0€
[INFO]:   - energy_demand:                 101.85kWh
[INFO]:   - energy_production:             0.0kWh
[INFO]:   - storages_energy_demand:        0.0kWh
[INFO]:   - storages_energy_production:    0.0kWh
[INFO]:   - self_consumed_energy:          0.0kWh
[INFO]:   - grid_import_energy:            0.0kWh
[INFO]:   - grid_export_energy:            0.0kWh
[INFO]:   - community_import_energy:       101.85kWh
[INFO]:   - community_export_energy:       0.0kWh
[INFO]:   - grid_import_peak:              0.0kW at <2020-05-31 22:00:00> (previously: 9.1kW at <2020-06-01 08:00:00>) [-100.0%]
[INFO]:
[INFO]: [Member: MeryBois] from <2020-05-31 22:00:00> to <2020-06-01 21:00:00> (freq: <1H>)
[INFO]:   - total_energy_cost:             -17.26€
[INFO]:   - energy_demand:                 197.25kWh
[INFO]:   - energy_production:             618.47kWh
[INFO]:   - storages_energy_demand:        0.0kWh
[INFO]:   - storages_energy_production:    0.0kWh
[INFO]:   - self_consumed_energy:          136.79kWh
[INFO]:   - grid_import_energy:            0.0kWh
[INFO]:   - grid_export_energy:            373.89kWh
[INFO]:   - community_import_energy:       60.46kWh
[INFO]:   - community_export_energy:       107.79kWh
[INFO]:   - grid_import_peak:              0.0kW at <2020-05-31 22:00:00> (previously: 16.9kW at <2020-06-01 03:00:00>) [-100.0%]
[INFO]:
[INFO]: [Member: MeryTherm] from <2020-05-31 22:00:00> to <2020-06-01 21:00:00> (freq: <1H>)
[INFO]:   - total_energy_cost:             -4.43€
[INFO]:   - energy_demand:                 1832.46kWh
[INFO]:   - energy_production:             1886.88kWh
[INFO]:   - storages_energy_demand:        0.0kWh
[INFO]:   - storages_energy_production:    0.0kWh
[INFO]:   - self_consumed_energy:          1685.46kWh
[INFO]:   - grid_import_energy:            0.0kWh
[INFO]:   - grid_export_energy:            95.96kWh
[INFO]:   - community_import_energy:       147.0kWh
[INFO]:   - community_export_energy:       105.47kWh
[INFO]:   - grid_import_peak:              0.0kW at <2020-05-31 22:00:00> (previously: 38.33kW at <2020-06-01 18:00:00>) [-100.0%]
[INFO]:
[INFO]: [Storage: ECM Battery] from <2020-05-31 22:00:00> to <2020-06-01 21:00:00> (freq: <1H>)
[INFO]:   - state_of_charge:               start: 50.0%, end: 10.0%, avg: 48.39%
[INFO]:   - charge_power_setpoints:        avg:   -4.0kW
[INFO]:
[INFO]: Writing results in file "out_microgrid_simulation.dat"...
[INFO]: Results have been exported in file "out_microgrid_simulation.dat".
[INFO]:
```

You can now submit the simulations to your docker machine as follows:

```bash

# (Optional) Set up your docker context [replace 'default' by your context name]
docker context use default

# --- (project: merygrid, date: 2020-06) ---

# Run simulations on a docker machine
./merygrid/scripts/simulate-2020-06.sh

# Retrieve results from docker containers
./merygrid/scripts/get_results-2020-06.sh # saved in ./merygrid/results/

# Compute REC billing (executed locally)
./merygrid/scripts/export_billing-2020-06.sh # saved in ./merygrid/exports/
```
