# [BUILD] #####################################################################
FROM python:3.7 AS build

# Setup work directory for build
ADD ./packages /app/packages
WORKDIR /app/packages

# Install
RUN pip install --upgrade pip
RUN pip install *tar.gz


# [FINAL] #####################################################################
FROM python:3.7 AS final

# Copy files from build
COPY --from=build /usr/local/lib/python3.7/site-packages/ /usr/local/lib/python3.7/site-packages/

# Setup work directory for final
ADD ./projects.tar /app/
ADD  ./solvers /app/solvers
WORKDIR /app

# Install solver dependencies
RUN apt update && apt -y --allow-unauthenticated install gfortran

# Set up PATH and LD_LIBRARY_PATH in order to reference the solver(s)
ENV PATH /app/solvers/cbc/bin/:$PATH
ENV LD_LIBRARY_PATH /app/solvers/cbc/lib/:$LD_LIBRARY_PATH
ENV PATH /app/solvers/ipopt/bin/:$PATH
ENV PATH /app/solvers/cplex/bin/:$PATH

# Set run command
CMD python -m uliege.microgrid --help
