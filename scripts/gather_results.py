#
# Example usage:
#
#    python scripts/gather_results.py \
#        --projects merygrid merygrid_pv merygrid_wt \
#        --output_path tables/ \
#        --community_dso_fees 0.0 0.055 0.11
#
import sys
import os
from typing import List, Dict, Union
import shutil
import glob
import argparse
import pandas as pd
import pickle
import re

from uliege.microgrid.engine import MicrogridRealisation, NoRECMicrogridRealisation


loaded_realisations: Dict[str, MicrogridRealisation] = {}  # memoization
loaded_billing_df: Dict[str, pd.DataFrame] = {}  # memoization


def gather_results(
    projects: List[str],
    dates: List[str],
    output_path: str,
    community_dso_fees: List[float],
    disable_summary_table: bool = False,
    disable_energy_exchanges_table: bool = False,
    disable_peaks_table: bool = False,
    disable_costs_table: bool = False,
):
    print("Args:")
    print(f"  * projects:")
    for project in projects:
        print(f"    - {project}")
    print(f"  * output_path: {output_path}")
    print("")

    if os.path.exists(output_path):
        if query_yes_no(
            f"Directory {output_path} will be deleted, do you want to continue?"
        ):
            shutil.rmtree(output_path)
            print("")
        else:
            exit()
    os.makedirs(output_path)

    paths = glob.glob(f"{project}/results/*")
    if dates:
        paths = [path for path in paths for date in dates if date in path]

    paths_by_season = {
        "summer": [path for path in paths if re.search(".-(07|08|09)", path)],
        "autumn": [path for path in paths if re.search(".-(10|11|12)", path)],
        "winter": [path for path in paths if re.search(".-(01|02|03)", path)],
        "spring": [path for path in paths if re.search(".-(04|05|06)", path)],
    }

    for project in projects:
        # Summary table
        if not disable_summary_table:
            for project_suffix in ["", "_static_evs", "_evs"]:
                with pd.ExcelWriter(
                    f"{output_path}/{project}_table1_summary{project_suffix}.xlsx"
                ) as writer:
                    # --- GLOBAL
                    df = build_summary_table(
                        project=f"{project}{project_suffix}",
                        dates=[path.split("/")[-1] for path in paths],
                    )
                    df.to_excel(writer, sheet_name="full_year")

                    # -- BY SEASON
                    for season, season_paths in paths_by_season.items():
                        df = build_summary_table(
                            project=f"{project}{project_suffix}",
                            dates=[path.split("/")[-1] for path in season_paths],
                        )
                        df.to_excel(writer, sheet_name=season)

                    # --- ONE PER DATE
                    for path in sorted(paths):
                        df = build_summary_table(
                            project=f"{project}{project_suffix}",
                            dates=[path.split("/")[-1]],
                        )
                        df.to_excel(writer, sheet_name=df.index.name)

        # Energy exchanges table
        if not disable_energy_exchanges_table:
            for project_suffix in ["", "_static_evs", "_evs"]:
                with pd.ExcelWriter(
                    f"{output_path}/{project}_table2_energy_exchanges{project_suffix}.xlsx"
                ) as writer:
                    # --- GLOBAL
                    df = build_energy_exchanges_table(
                        project=f"{project}{project_suffix}",
                        dates=[path.split("/")[-1] for path in paths],
                    )
                    df.to_excel(writer, sheet_name="full_year")

                    # --- BY SEASON
                    for season, season_paths in paths_by_season.items():
                        df = build_energy_exchanges_table(
                            project=f"{project}{project_suffix}",
                            dates=[path.split("/")[-1] for path in season_paths],
                        )
                        df.to_excel(writer, sheet_name=season)

                    # --- ONE PER DATE
                    for path in sorted(paths):
                        df = build_energy_exchanges_table(
                            project=f"{project}{project_suffix}",
                            dates=[path.split("/")[-1]],
                        )
                        df.to_excel(writer, sheet_name=df.index.name)

        # Import Peaks table
        if not disable_peaks_table:
            with pd.ExcelWriter(f"{output_path}/{project}_table3_peaks.xlsx") as writer:
                # --- ONE PER DATE
                for path in sorted(paths):
                    df = build_peaks_table(
                        project=project,
                        date=path.split("/")[-1],
                    )
                    df.to_excel(writer, sheet_name=df.index.name)

        # Costs table
        if not disable_costs_table:
            for community_dso_fee in community_dso_fees:
                filename: str = f"{project}_table4_costs"
                if community_dso_fee is not None:
                    filename += f"_[community_dso_fee={community_dso_fee}]"

                with pd.ExcelWriter(f"{output_path}/{filename}.xlsx") as writer:
                    # --- GLOBAL
                    df = build_costs_table(
                        project=project,
                        dates=[path.split("/")[-1] for path in paths],
                        community_dso_fee=community_dso_fee,
                    )
                    df.to_excel(writer, sheet_name=df.index.name)

                    # --- BY SEASON
                    for season, season_paths in paths_by_season.items():
                        df = build_costs_table(
                            project=project,
                            dates=[path.split("/")[-1] for path in season_paths],
                            community_dso_fee=community_dso_fee,
                        )
                        df.to_excel(writer, sheet_name=season)

                    # --- ONE PER DATE
                    for path in sorted(paths):
                        df = build_costs_table(
                            project=project,
                            dates=[path.split("/")[-1]],
                            community_dso_fee=community_dso_fee,
                        )
                        df.to_excel(writer, sheet_name=df.index.name)


# Summary table
def build_summary_table(project: str, dates: List[str]) -> pd.DataFrame:
    dates.sort()

    # Build intermediate df
    df_to_merge: List[pd.DataFrame] = []
    for date in dates:
        realisation: MicrogridRealisation = load_realisation(
            f"{project}/results/{date}/out_microgrid_simulation.dat"
        )
        df: pd.DataFrame = pd.DataFrame(
            data={
                member_realisation.member.name: [
                    sum(member_realisation.energy_demand),
                    sum(member_realisation.energy_production),
                    sum(
                        member_realisation.energy_demand
                        - member_realisation.energy_production
                    ),
                ]
                for member_realisation in realisation.member_realisations
            },
            index=[
                "Consommation",
                "Production",
                "Net\nConsommation - Production",
            ],
        )
        dt = pd.Timestamp(date)
        df["Total"] = df[df.columns].sum(axis=1)
        df_to_merge.append(df)

    # Consolidate all df into a single one
    df: pd.DataFrame = sum(df for df in df_to_merge)

    # Set index name
    dt = pd.Timestamp(dates[0])
    df.index.name = f"{dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
    if len(dates) > 1:
        dt = pd.Timestamp(dates[-1])
        df.index.name += (
            f" --> {dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
        )

    # Return
    return df


# Energy exchanges table
def build_energy_exchanges_table(project: str, dates: List[str]) -> pd.DataFrame:
    dates.sort()

    # Build intermediate df
    df_to_merge: List[pd.DataFrame] = []
    for date in dates:
        realisation_no_rec: MicrogridRealisation = load_realisation(
            f"{project}/results/{date}/out_microgrid_no_REC_simulation.dat"
        )
        realisation_no_battery: MicrogridRealisation = load_realisation(
            f"{project}/results/{date}/out_microgrid_no_battery_simulation.dat"
        )
        realisation: MicrogridRealisation = load_realisation(
            f"{project}/results/{date}/out_microgrid_simulation.dat"
        )
        realisation_forecast: MicrogridRealisation = load_realisation(
            f"{project}_forecast/results/{date}/out_microgrid_simulation.dat"
        )

        df: pd.DataFrame = pd.DataFrame(
            data={
                "Sans Batterie\nSans CER": [
                    sum(
                        sum(member_realisation.self_consumed_energy)
                        for member_realisation in realisation_no_rec.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_import_energy)
                        for member_realisation in realisation_no_rec.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_export_energy)
                        for member_realisation in realisation_no_rec.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_import_energy)
                        for member_realisation in realisation_no_rec.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_export_energy)
                        for member_realisation in realisation_no_rec.member_realisations
                    ),
                ],
                "Sans Batterie\nOptimal Théorique": [
                    sum(
                        sum(member_realisation.self_consumed_energy)
                        for member_realisation in realisation_no_battery.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_import_energy)
                        for member_realisation in realisation_no_battery.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_export_energy)
                        for member_realisation in realisation_no_battery.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_import_energy)
                        for member_realisation in realisation_no_battery.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_export_energy)
                        for member_realisation in realisation_no_battery.member_realisations
                    ),
                ],
                "Avec Batterie\nOptimal Théorique": [
                    sum(
                        sum(member_realisation.self_consumed_energy)
                        for member_realisation in realisation.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_import_energy)
                        for member_realisation in realisation.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_export_energy)
                        for member_realisation in realisation.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_import_energy)
                        for member_realisation in realisation.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_export_energy)
                        for member_realisation in realisation.member_realisations
                    ),
                ],
                "Avec Batterie\nRéalité": [
                    sum(
                        sum(member_realisation.self_consumed_energy)
                        for member_realisation in realisation_forecast.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_import_energy)
                        for member_realisation in realisation_forecast.member_realisations
                    ),
                    sum(
                        sum(member_realisation.grid_export_energy)
                        for member_realisation in realisation_forecast.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_import_energy)
                        for member_realisation in realisation_forecast.member_realisations
                    ),
                    sum(
                        sum(member_realisation.community_export_energy)
                        for member_realisation in realisation_forecast.member_realisations
                    ),
                ],
            },
            index=[
                "Auto-consommation",
                "Energie importée du réseau",
                "Energie exportée vers le réseau",
                "Energie importée de la communauté",
                "Energie exportée vers la communauté",
            ],
        )
        dt = pd.Timestamp(date)
        df_to_merge.append(df)

    # Consolidate all df into a single one
    df: pd.DataFrame = sum(df for df in df_to_merge)

    # Set index name
    dt = pd.Timestamp(dates[0])
    df.index.name = f"{dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
    if len(dates) > 1:
        dt = pd.Timestamp(dates[-1])
        df.index.name += (
            f" --> {dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
        )

    # Return
    return df


# Import Peaks table
def build_peaks_table(project: str, date: str) -> pd.DataFrame:
    df: pd.DataFrame = pd.DataFrame(
        data={
            "Sans Batterie\nSans CER": [
                load_realisation(
                    f"{project}/results/{date}/out_microgrid_no_rec_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_static_evs/results/{date}/out_microgrid_no_rec_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_evs/results/{date}/out_microgrid_no_rec_simulation.dat"
                ).grid_import_peak[1],
            ],
            "Sans Batterie\nOptimal Théorique": [
                load_realisation(
                    f"{project}/results/{date}/out_microgrid_no_battery_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_static_evs/results/{date}/out_microgrid_no_battery_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_evs/results/{date}/out_microgrid_no_battery_simulation.dat"
                ).grid_import_peak[1],
            ],
            "Avec Batterie\nOptimal Théorique": [
                load_realisation(
                    f"{project}/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_static_evs/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_evs/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
            ],
            "Avec Batterie\nRéalité": [
                load_realisation(
                    f"{project}_forecast/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_static_evs_forecast/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
                load_realisation(
                    f"{project}_evs_forecast/results/{date}/out_microgrid_simulation.dat"
                ).grid_import_peak[1],
            ],
        },
        index=[
            "Sans VE",
            "Avec VE (sans smart-charging)",
            "Avec VE (avec smart-charging)",
        ],
    )

    # Set index name
    dt = pd.Timestamp(date)
    df.index.name = f"{dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"

    # Return
    return df


# Costs table
def build_costs_table(
    project: str, dates: List[str], community_dso_fee: float
) -> pd.DataFrame:

    for date in dates:
        dates.sort()

        realisation: MicrogridRealisation = load_realisation(
            f"{project}/results/{date}/out_microgrid_simulation.dat"
        )
        member_names = [
            member.name
            for member in realisation.microgrid.community.members_and_manager
        ]

        # Build intermediate df
        df_to_merge: List[pd.DataFrame] = []
        for date in dates:
            df: pd.DataFrame = pd.DataFrame(
                data={
                    "Sans Batterie\nSans CER": [
                        get_total_cost(
                            member_names, project, "_no_rec", date, community_dso_fee
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_static_evs",
                            "_no_rec",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_evs",
                            "_no_rec",
                            date,
                            community_dso_fee,
                        ),
                    ],
                    "Sans Batterie\nOptimal Théorique": [
                        get_total_cost(
                            member_names,
                            project,
                            "_no_battery",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_static_evs",
                            "_no_battery",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_evs",
                            "_no_battery",
                            date,
                            community_dso_fee,
                        ),
                    ],
                    "Avec Batterie\nOptimal Théorique": [
                        get_total_cost(
                            member_names, project, "", date, community_dso_fee
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_static_evs",
                            "",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names, f"{project}_evs", "", date, community_dso_fee
                        ),
                    ],
                    "Avec Batterie\nRéalité": [
                        get_total_cost(
                            member_names,
                            f"{project}_forecast",
                            "",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_static_evs_forecast",
                            "",
                            date,
                            community_dso_fee,
                        ),
                        get_total_cost(
                            member_names,
                            f"{project}_evs_forecast",
                            "",
                            date,
                            community_dso_fee,
                        ),
                    ],
                },
                index=[
                    "Sans VE",
                    "Avec VE (sans smart-charging)",
                    "Avec VE (avec smart-charging)",
                ],
            )
            df_to_merge.append(df)

    # Consolidate all df into a single one
    df: pd.DataFrame = sum(df for df in df_to_merge)

    # Set index name
    dt = pd.Timestamp(dates[0])
    df.index.name = f"{dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
    if len(dates) > 1:
        dt = pd.Timestamp(dates[-1])
        df.index.name += (
            f" --> {dt.month_name(locale='fr_BE.UTF-8')} {dt.strftime('%Y')}"
        )

    # Return
    return df


def get_total_cost(
    member_names: List[str], project: str, case: str, date: str, community_dso_fee
):
    return sum(
        get_member_total_cost(project, case, date, member_name)
        - get_member_community_dso_cost(project, case, date, member_name)
        + get_member_community_dso_cost(
            project, case, date, member_name, community_dso_fee
        )
        for member_name in member_names
    )


def get_member_total_cost(project: str, case: str, date: str, member_name: str):
    df: pd.DataFrame = load_billing_df(
        f"{project}/exports/{date}/out_microgrid{case}_billing.xlsx",
        "member_billing_summary",
    )
    return df[member_name]["total_cost"]


def get_member_community_dso_cost(
    project: str, case: str, date: str, member_name: str, dso_fee: float = None
):
    df: pd.DataFrame = load_billing_df(
        f"{project}/exports/{date}/out_microgrid{case}_billing.xlsx", "member_billing"
    )
    community_dso_cost: float = df[member_name]["community_dso_cost"]

    if dso_fee is not None:
        old_dso_fee: float = get_member_community_dso_fee(
            project, case, date, member_name
        )
        community_dso_cost = community_dso_cost / (
            old_dso_fee if old_dso_fee != 0.0 else 1.0
        )
        community_dso_cost *= dso_fee

    return community_dso_cost


def get_member_community_dso_fee(project: str, case: str, date: str, member_name: str):
    df: pd.DataFrame = load_billing_df(
        f"{project}/exports/{date}/out_microgrid{case}_billing.xlsx", "community_prices"
    )
    return df[member_name]["dso_fee"]


# https://stackoverflow.com/a/3041990
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


def load_realisation(path: str) -> MicrogridRealisation:  # memoization
    key: str = path
    if loaded_realisations.get(key) is None:
        loaded_realisations[key] = pickle.load(open(path, "rb"))
        if "no_REC" in path:
            loaded_realisations[key] = NoRECMicrogridRealisation(
                loaded_realisations[key]
            )

    return loaded_realisations[key]


def load_billing_df(path: str, sheet_name: str) -> pd.DataFrame:  # memoization
    key: str = f"{path}_{sheet_name}"
    if loaded_billing_df.get(key) is None:
        loaded_billing_df[key] = pd.read_excel(
            pd.ExcelFile(path), sheet_name=sheet_name, index_col=0
        )

    return loaded_billing_df[key]


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--projects", nargs="+")
    parser.add_argument("--dates", nargs="+")
    parser.add_argument("--output_path", type=str, required=True)
    parser.add_argument("--community_dso_fees", type=float, nargs="+", default=[None])
    parser.add_argument("--disable_summary_table", action="store_true")
    parser.add_argument("--disable_energy_exchanges_table", action="store_true")
    parser.add_argument("--disable_peaks_table", action="store_true")
    parser.add_argument("--disable_costs_table", action="store_true")
    args = parser.parse_args()

    gather_results(
        projects=args.projects,
        dates=args.dates,
        output_path=args.output_path,
        community_dso_fees=args.community_dso_fees,
        disable_summary_table=args.disable_summary_table,
        disable_energy_exchanges_table=args.disable_energy_exchanges_table,
        disable_peaks_table=args.disable_peaks_table,
        disable_costs_table=args.disable_costs_table,
    )
