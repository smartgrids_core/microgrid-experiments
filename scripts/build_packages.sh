#!/bin/bash
set -e

{ # try
    python -c 'import sys; assert sys.version_info >= (3, 7)' > /dev/null 2>&1
} || { # catch
    echo ""
    echo "Python version is incorrect (expected version: 3.7.*)"
    echo ""
    exit -1
}

# Clean packages/ folder
if [ -d "packages/" ]; then
    rm -rf packages/
fi
mkdir -p packages/

echo ""
for repository_name in 'forecast'; do
    if [ ! -d "dependencies/${repository_name}" ]; then
        echo ""
        echo "'${repository_name}' repository is missing, please run the following command from folder 'dependencies/':"
        echo "  ==> git clone https://gitlab.uliege.be/ems/${repository_name}.git"
        echo ""
        exit -1
    fi

    (cd dependencies/${repository_name}; echo "Git pull '${repository_name}'..."; git pull)
    (rm -f dependencies/${repository_name}/dist/*tar.gz)
    (cd dependencies/${repository_name}; python setup.py sdist)
    (cp dependencies/${repository_name}/dist/*tar.gz packages/)
    echo ""

done
for repository_name in 'dataframe_pipeline' 'decision_process' 'decision_process_collection' 'microgrid' 'evkit'; do
    if [ ! -d "dependencies/${repository_name}" ]; then
        echo ""
        echo "'${repository_name}' repository is missing, please run the following command from folder 'dependencies/':"
        echo "  ==> git clone https://<USERNAME>@bitbucket.org/smartgrids_core/${repository_name}.git"
        echo ""
        exit -1
    fi

    (cd dependencies/${repository_name}; echo "Git pull '${repository_name}'..."; git pull)
    (rm -f dependencies/${repository_name}/dist/*tar.gz)
    (cd dependencies/${repository_name}; python setup.py sdist)
    (cp dependencies/${repository_name}/dist/*tar.gz packages/)
    echo ""

done
