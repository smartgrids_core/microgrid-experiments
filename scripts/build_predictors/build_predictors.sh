#!/bin/bash
set -e

echo "[PREDICTORS]"
mkdir -p forecast/

# Export training data into CSV (= learning set)
echo "Export training data into CSV..."
python -m uliege.microgrid export-data-supplier \
    --microgrid ./scripts/build_predictors/microgrid.json \
    --xlsx_microgrid_data_supplier "data/2020-06_2021-05.xlsx" \
    --start_date "2020-06-01 00:00:00" \
    --periods 35032 \
    --period_duration 15 \
    --period_duration_unit minute \
    --pattern '.*: ACTIVE_POWER' \
    --output forecast/data/ls_data_15T.csv \
    --output_format csv

# Build predictors
echo "Build predictors..."
for name in templates/merygrid_forecast/var_*-*.yml; do
    label=$(echo $name | awk -F '/' '{print $3}' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}')
    end=$(python -c "import pandas as pd; print(str(pd.Timestamp('${label}') - pd.Timedelta('15T')).split(' ')[0])")
    start=$(echo $end | awk -F '-' '{print $1"-"$2"-01"}')
    predictor_label=$(echo $start | awk -F '-' '{print $1"-"$2}')

    # Train forecasters to predict series (freq: 15T, horizon: 24H)

    # # CBV
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_cbv_load_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "CBV [load]: ACTIVE_POWER" \
    #     --target-field "CBV [load]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"
    
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_cbv_ev_fleet_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "CBV [EV fleet]: ACTIVE_POWER" \
    #     --target-field "CBV [EV fleet]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"
    
    python -m uliege.forecast train \
        --output-model-path forecast/predictor/${predictor_label}_cbv_pv_prod_15T_24H/ \
        --input-data-file forecast/data/ls_data_15T.csv \
        --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
        --input-field "CBV_PV [prod]: ACTIVE_POWER" \
        --target-field "CBV_PV [prod]: ACTIVE_POWER" \
        --start-date "${start} 00:00:00" \
        --fetch-end-date "${end} 23:45:00" \
        --freq "15min"

    # # MeryBois
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merybois_load_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryBois [load]: ACTIVE_POWER" \
    #     --target-field "MeryBois [load]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"
    
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merybois_ev_fleet_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryBois [EV fleet]: ACTIVE_POWER" \
    #     --target-field "MeryBois [EV fleet]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"

    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merybois_prod_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryBois [prod]: ACTIVE_POWER" \
    #     --target-field "MeryBois [prod]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"

    # # MeryTherm
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merytherm_load_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryTherm [load]: ACTIVE_POWER" \
    #     --target-field "MeryTherm [load]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"
    
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merytherm_ev_fleet_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryTherm [EV fleet]: ACTIVE_POWER" \
    #     --target-field "MeryTherm [EV fleet]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"

    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merytherm_prod_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryTherm [prod]: ACTIVE_POWER" \
    #     --target-field "MeryTherm [prod]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"
    
    python -m uliege.forecast train \
        --output-model-path forecast/predictor/${predictor_label}_merytherm_pv_prod_15T_24H/ \
        --input-data-file forecast/data/ls_data_15T.csv \
        --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
        --input-field "MeryTherm_PV [prod]: ACTIVE_POWER" \
        --target-field "MeryTherm_PV [prod]: ACTIVE_POWER" \
        --start-date "${start} 00:00:00" \
        --fetch-end-date "${end} 23:45:00" \
        --freq "15min"

    # # MeryWind
    # python -m uliege.forecast train \
    #     --output-model-path forecast/predictor/${predictor_label}_merywind_prod_15T_24H/ \
    #     --input-data-file forecast/data/ls_data_15T.csv \
    #     --training-algorithm cnn prediction_length=96 context_length=480 evaluator.seasonality=672 trainer.epochs=20 freq="15min" \
    #     --input-field "MeryWind [prod]: ACTIVE_POWER" \
    #     --target-field "MeryWind [prod]: ACTIVE_POWER" \
    #     --start-date "${start} 00:00:00" \
    #     --fetch-end-date "${end} 23:45:00" \
    #     --freq "15min"

done
