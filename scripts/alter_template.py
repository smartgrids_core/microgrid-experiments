import sys
import os
from pathlib import Path
import shutil
import glob
import argparse
import pandas as pd


def alter_template(
    template_path: str,
    use_controller_microgrid_data_supplier: bool,
    use_static_evs: bool,
    n_evs: int,
    output_path: str,
):
    print("Args:")
    print(f"  * template_path:                          {template_path}")
    print(
        f"  * use_controller_microgrid_data_supplier: {use_controller_microgrid_data_supplier}"
    )
    print(f"  * use_static_evs:                         {use_static_evs}")
    print(f"  * n_evs:                                  {n_evs}")
    print(f"  * output_path:                            {output_path}")
    print("")

    if os.path.exists(output_path):
        if query_yes_no(
            f"Directory {output_path} will be deleted, do you want to continue?"
        ):
            shutil.rmtree(output_path)
        else:
            exit()
    os.mkdir(output_path)

    # var_[!20*]*.yml files (common config files)
    for filepath in glob.glob(f"{template_path}/var_[!20*]*.yml"):
        lines = []
        with open(filepath, "r") as file:
            for line in file.readlines():
                if "n_evs: " in line:
                    line = f"{line.split(':')[0]}: {n_evs}\n"
                if "use_static_evs: " in line:
                    line = (
                        f"{line.split(':')[0]}: "
                        f"{'yes' if use_static_evs else 'no'}\n"
                    )

                lines.append(line)

        with open(filepath.replace(template_path, output_path, 1), "w") as output_file:
            output_file.writelines(lines)

    # var_20*.yml files (month config files)
    filepaths = glob.glob(f"{template_path}/var_20*.yml")
    if use_controller_microgrid_data_supplier:  # skip earliest (no forecast available)
        filepaths.sort()
        filepaths = filepaths[1:]

    for filepath in filepaths:
        lines = []
        with open(filepath, "r") as file:
            for line in file.readlines():
                if "use_controller_microgrid_data_supplier: " in line:
                    date = filepath.split("var_")[1].split(".yml")[0]
                    predictor_date = previous_month_str(date)

                    if use_controller_microgrid_data_supplier:
                        line = (
                            "use_controller_microgrid_data_supplier: yes\n"
                            f"PREDICTOR_DATE: {predictor_date}\n\n"
                        )
                    else:
                        line = "use_controller_microgrid_data_supplier: no\n"

                lines.append(line)

        with open(filepath.replace(template_path, output_path, 1), "w") as output_file:
            output_file.writelines(lines)

    # copy controller_microgrid_data_supplier.json.j2 (if needed)
    if use_controller_microgrid_data_supplier:
        shutil.copy(
            f"{str(Path(template_path).parent)}/controller_microgrid_data_supplier.json.j2",
            output_path,
        )


def previous_month_str(s: str) -> str:
    return (pd.Timestamp(s).replace(day=1) - pd.Timedelta("1 day")).strftime("%Y-%m")


# https://stackoverflow.com/a/3041990
def query_yes_no(question, default="yes"):
    """Ask a yes/no question via raw_input() and return their answer.

    "question" is a string that is presented to the user.
    "default" is the presumed answer if the user just hits <Enter>.
            It must be "yes" (the default), "no" or None (meaning
            an answer is required of the user).

    The "answer" return value is True for "yes" or False for "no".
    """
    valid = {"yes": True, "y": True, "ye": True, "no": False, "n": False}
    if default is None:
        prompt = " [y/n] "
    elif default == "yes":
        prompt = " [Y/n] "
    elif default == "no":
        prompt = " [y/N] "
    else:
        raise ValueError("invalid default answer: '%s'" % default)

    while True:
        sys.stdout.write(question + prompt)
        choice = input().lower()
        if default is not None and choice == "":
            return valid[default]
        elif choice in valid:
            return valid[choice]
        else:
            sys.stdout.write("Please respond with 'yes' or 'no' " "(or 'y' or 'n').\n")


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--template_path", type=str, required=True)
    parser.add_argument("--use_controller_microgrid_data_supplier", action="store_true")
    parser.add_argument("--use_static_evs", action="store_true")
    parser.add_argument("--n_evs", type=int, default=0)
    parser.add_argument("--output_path", type=str, required=True)
    args = parser.parse_args()

    alter_template(
        template_path=args.template_path,
        use_controller_microgrid_data_supplier=args.use_controller_microgrid_data_supplier,
        use_static_evs=args.use_static_evs,
        n_evs=args.n_evs,
        output_path=args.output_path,
    )
