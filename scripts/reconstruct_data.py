import os
from typing import Dict, List
from datetime import time, timedelta, tzinfo
import pandas as pd

from uliege.evkit.strategy import DummyStrategy
from uliege.evkit.express import generate


def elia_cet_to_utc(index: pd.Series, rule: str = "15T") -> pd.DatetimeIndex:
    start_dt: pd.Timestamp = str_to_timestamp(index[0], "Europe/Brussels").tz_convert(
        None
    )
    end_dt: pd.Timestamp = str_to_timestamp(index[-1], "Europe/Brussels").tz_convert(
        None
    )
    return pd.date_range(start=start_dt, end=end_dt, freq=rule)


def str_to_timestamp(s: str, tz) -> pd.Timestamp:
    # e.g.: 30/06/2020 22:45
    day: str = s.split("/")[0]
    month: str = s.split("/")[1]
    year: str = s.split("/")[2].split(" ")[0]
    hour: str = s.split("/")[2].split(" ")[1].split(":")[0]
    minute: str = s.split("/")[2].split(" ")[1].split(":")[1]

    return pd.Timestamp(f"{year}-{month}-{day} {hour}:{minute}", tz=tz).round("T")


# Entrypoint
if __name__ == "__main__":
    # Load data from merygrid project
    #
    #   in power:
    #    'CBV [global]: ACTIVE_POWER',
    #    'MeryBois [load][estimated]: ACTIVE_POWER',
    #    'MeryTherm [four]: ACTIVE_POWER', 'MeryBois [prod]: ACTIVE_POWER',
    #    'MeryTherm [prod]: ACTIVE_POWER', 'CBV [load][clean]: ACTIVE_POWER',
    #    'CBV [net][RESA]: ACTIVE_POWER', 'MeryBois [load][clean]: ACTIVE_POWER',
    #    'MeryBois [prod][clean]: ACTIVE_POWER',
    #    'MeryBois [net][RESA]: ACTIVE_POWER',
    #    'MeryTherm [load][clean]: ACTIVE_POWER',
    #    'MeryTherm [prod][clean]: ACTIVE_POWER',
    #    'MeryTherm [net][RESA]: ACTIVE_POWER',
    #    'CBV [load][reconstructed]: ACTIVE_POWER',
    #    'MeryBois [load][reconstructed]: ACTIVE_POWER',
    #    'MeryBois [prod][reconstructed]: ACTIVE_POWER',
    #    'MeryTherm [load][reconstructed]: ACTIVE_POWER',
    #    'MeryTherm [prod][reconstructed]: ACTIVE_POWER'
    #
    data: Dict[str, pd.DataFrame] = pd.read_excel(
        pd.ExcelFile("data/2020-06_2021-05_merygrid.xlsx"),
        sheet_name=None,
        index_col=0,
    )

    # Load data from elia
    #
    #   Aggregated wind turbine generation (onshore)
    #
    elia_data: Dict[str, pd.DataFrame] = {}
    elia_folderpath: str = "data/elia_wind_turbine_generation_onshore/"
    filenames = os.listdir(elia_folderpath)

    prod_series_to_concat: List[pd.Series] = []
    capacity_series_to_concat: List[pd.Series] = []
    for filename in filenames:
        df: pd.DataFrame = pd.read_excel(
            pd.ExcelFile(f"{elia_folderpath}/{filename}"),
            sheet_name="forecast",
            index_col=0,
        )
        prod_series: pd.Series = df["Unnamed: 4"].iloc[3:]
        prod_series.index.name = "datetime"
        prod_series.index = elia_cet_to_utc(prod_series.index.to_series())
        prod_series.name = df["Unnamed: 4"].iloc[2]
        assert prod_series.name == "Measured & upscaled [MW]"

        capacity_series: pd.Series = df["Unnamed: 5"].iloc[3:]
        capacity_series.index.name = "datetime"
        capacity_series.index = elia_cet_to_utc(capacity_series.index.to_series())
        capacity_series.name = df["Unnamed: 5"].iloc[2]
        assert capacity_series.name == "Monitored Capacity [MW]"

        assert (prod_series <= capacity_series).all()

        prod_series_to_concat.append(prod_series)
        capacity_series_to_concat.append(capacity_series)

    prod_series: pd.Series = pd.concat(prod_series_to_concat).sort_index()
    prod_series = prod_series[~prod_series.index.duplicated(keep="first")]
    prod_series.name = "Wind Turbines (Onshore): ACTIVE_POWER"

    capacity_series: pd.Series = pd.concat(capacity_series_to_concat).sort_index()
    capacity_series = capacity_series[~capacity_series.index.duplicated(keep="first")]
    capacity_series.name = "Wind Turbines (Onshore): CAPACITY"

    elia_data["Wind Turbines (Onshore): ACTIVE_POWER"] = prod_series * 1000  # MW -> kW
    elia_data["Wind Turbines (Onshore): CAPACITY"] = (
        capacity_series * 1000
    )  # MWh -> kWh

    # Series for a static EV fleet
    ev_fleet_series: pd.DataFrame = generate(
        strategy=DummyStrategy(),
        n_evs=5,
        ev_capacity=60,
        ev_charging_efficiency=0.9,
        ev_initial_arrival_time=time(hour=9),
        ev_initial_arrival_time_noise=timedelta(hours=0),
        ev_initial_state_of_charge=0.2,
        ev_initial_state_of_charge_noise=0.0,
        ev_target_state_of_charge=0.8,
        charging_point_power=7.4,
        start_date=data["power"].index[0],
        end_date=data["power"].index[-1],
        rule=pd.infer_freq(data["power"].index),
    )

    # Copy all sheets except 'power' sheet
    n_data: Dict[str, pd.DataFrame] = {}
    for sheet_name, sheet in data.items():
        if sheet_name != "power":
            n_data[sheet_name] = sheet

    # Create a custom 'power' sheet
    power_df: pd.DataFrame = data["power"]
    n_power_df: pd.DataFrame = pd.DataFrame(index=power_df.index)

    # CBV
    n_power_df["CBV [load]: ACTIVE_POWER"] = power_df[
        "CBV [load][reconstructed]: ACTIVE_POWER"
    ]
    n_power_df["CBV_PV [prod]: ACTIVE_POWER"] = (
        power_df["MeryBois [prod][reconstructed]: ACTIVE_POWER"] / 60.0
    ) * 70.0
    n_power_df["CBV [EV fleet]: ACTIVE_POWER"] = ev_fleet_series

    # MeryBois
    n_power_df["MeryBois [load]: ACTIVE_POWER"] = power_df[
        "MeryBois [load][reconstructed]: ACTIVE_POWER"
    ]
    n_power_df["MeryBois [EV fleet]: ACTIVE_POWER"] = ev_fleet_series
    n_power_df["MeryBois [prod]: ACTIVE_POWER"] = power_df[
        "MeryBois [prod][reconstructed]: ACTIVE_POWER"
    ]

    # MeryTherm
    n_power_df["MeryTherm [load]: ACTIVE_POWER"] = power_df[
        "MeryTherm [load][reconstructed]: ACTIVE_POWER"
    ]
    n_power_df["MeryTherm_PV [prod]: ACTIVE_POWER"] = (
        power_df["MeryBois [prod][reconstructed]: ACTIVE_POWER"] / 60.0
    ) * 130.0
    n_power_df["MeryTherm [EV fleet]: ACTIVE_POWER"] = ev_fleet_series
    n_power_df["MeryTherm [prod]: ACTIVE_POWER"] = power_df[
        "MeryTherm [prod][reconstructed]: ACTIVE_POWER"
    ]

    # MeryWind (used by ECM)
    #  https://images.renewableenergyworld.com/wp-content/uploads/2021/04/LCOE_WINDLBNL_Pic3_Lead.jpg
    #  onshore wt capacity: 2.5MW ~ 5.5MW
    #  If we had a wt of 2.6MW, we could serve 10 MeryGrid RECs (~30 members)
    #
    n_power_df["MeryWind [prod]: ACTIVE_POWER"] = (
        elia_data["Wind Turbines (Onshore): ACTIVE_POWER"]
        / elia_data["Wind Turbines (Onshore): CAPACITY"]
    ) * 260.0  # 70 + 60 + 130 kW

    n_data["power"] = n_power_df

    # Write a new file containing the new data
    with pd.ExcelWriter(
        "data/2020-06_2021-05.xlsx",
        datetime_format="yyyy-mm-ddTHH:MM:ssZ",
    ) as writer:
        for sheet_name, sheet in n_data.items():
            n_data[sheet_name].to_excel(writer, sheet_name=sheet_name)
