#!/bin/bash
set -e

# freq: 1H
export PERIODS_SCALING_FACTOR=0.25
export PERIOD_DURATION=1
export PERIOD_DURATION_UNIT='hour'

# # freq: 15M
# export PERIODS_SCALING_FACTOR=1.0
# export PERIOD_DURATION=15
# export PERIOD_DURATION_UNIT='minute'

echo ""
for project in $@; do
    echo "[PROJECT: ${project}]"
    export PROJECT_NAME=${project}

    if [ -d ${project}/results/ ]; then
        mv ${project}/results/ tmp.results/
    fi
    if [ -d ${project}/exports/ ]; then
        mv ${project}/exports/ tmp.exports/
    fi

    rm -rf ${project}
    mkdir -p ${project}
    
    if [ -d tmp.results/ ]; then
        mv tmp.results/ ${project}/results/
    fi
    if [ -d tmp.exports/ ]; then
        mv tmp.exports/ ${project}/exports/
    fi
    

    # Copy / Update folder 'data/'
    echo "Copy / Update folder 'data/'..."
    mkdir -p ${project}/data/
    cp data/2020-06_2021-05.xlsx ${project}/data/

    # Copy / Update folder 'controller/'
    echo "Copy / Update folder 'controller/'..."
    mkdir -p ${project}/controller/
    cp -r controller/*.json ${project}/controller/
    
    # Generate microgrid configuration files for simulations
    echo "Generate microgrid configuration files for simulations..."
    tmpfile=$(mktemp /tmp/microgrid.json.XXXXXX)

    # -- mode: SIMULATION
    export MODE="SIMULATION"
    j2 templates/microgrid.json.j2 templates/${project}/var_no_REC.yml > ${project}/data/microgrid_no_REC.json
    python -m json.tool ${project}/data/microgrid_no_REC.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid_no_REC.json

    j2 templates/microgrid.json.j2 templates/${project}/var_REC_no_battery.yml > ${project}/data/microgrid_no_battery.json
    python -m json.tool ${project}/data/microgrid_no_battery.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid_no_battery.json

    j2 templates/microgrid.json.j2 templates/${project}/var_REC.yml > ${project}/data/microgrid.json
    python -m json.tool ${project}/data/microgrid.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid.json

    # -- mode: BILLING
    export MODE="BILLING"
    j2 templates/microgrid.json.j2 templates/${project}/var_no_REC.yml > ${project}/data/microgrid_no_REC_billing.json
    python -m json.tool ${project}/data/microgrid_no_REC_billing.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid_no_REC_billing.json

    j2 templates/microgrid.json.j2 templates/${project}/var_REC_no_battery.yml > ${project}/data/microgrid_no_battery_billing.json
    python -m json.tool ${project}/data/microgrid_no_battery_billing.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid_no_battery_billing.json

    j2 templates/microgrid.json.j2 templates/${project}/var_REC.yml > ${project}/data/microgrid_billing.json
    python -m json.tool ${project}/data/microgrid_billing.json > ${tmpfile}
    mv ${tmpfile} ${project}/data/microgrid_billing.json

    # Setup for forecasts (if needed)
    if [ -f templates/${project}/controller_microgrid_data_supplier.json.j2 ]; then
        export USE_NODE1=yes
        export USE_NODE2=no
        export USE_NODE3=no

        echo "Generate controller data supplier config files..."
        tmpfile=$(mktemp /tmp/controller_microgrid_data_supplier.json.XXXXXX)

        for name in templates/${project}/var_202*-*.yml; do
            mkdir -p ${project}/forecast/

            label=$(echo $name | awk -F '/' '{print $3}' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}')
            predictor_label=$(grep 'PREDICTOR_DATE:' templates/${project}/var_${label}.yml | awk -F ' ' '{print $2}')
            j2 templates/${project}/controller_microgrid_data_supplier.json.j2 templates/${project}/var_${label}.yml > ${project}/forecast/controller_microgrid_data_supplier-${predictor_label}.json
            python -m json.tool ${project}/forecast/controller_microgrid_data_supplier-${predictor_label}.json > ${tmpfile}
            mv ${tmpfile} ${project}/forecast/controller_microgrid_data_supplier-${predictor_label}.json
        done

        echo "Copy predictors..."
        cp -r forecast/ ${project}/forecast/
    else
        export USE_NODE1=no
        export USE_NODE2=yes
        export USE_NODE3=no
    fi

    # Generate simulations scripts
    echo "Generate simulation scripts..."
    for name in templates/${project}/var_202*-*.yml; do
        mkdir -p ${project}/scripts/
        tmpfile=$(mktemp /tmp/simulation_script.sh.XXXXXX)
        label=$(echo $name | awk -F '/' '{print $3}' | awk -F '_' '{print $2}' | awk -F '.' '{print $1}')
        
        j2 templates/simulate.sh.j2 templates/${project}/var_${label}.yml > ${project}/scripts/simulate-$label.sh
        sed '/^$/N;/^\n$/D' ${project}/scripts/simulate-$label.sh > ${tmpfile}
        mv ${tmpfile} ${project}/scripts/simulate-$label.sh

        j2 templates/get_results.sh.j2 templates/${project}/var_${label}.yml > ${project}/scripts/get_results-$label.sh
        sed '/^$/N;/^\n$/D' ${project}/scripts/get_results-$label.sh > ${tmpfile}
        mv ${tmpfile} ${project}/scripts/get_results-$label.sh

        j2 templates/export_billing.sh.j2 templates/${project}/var_${label}.yml > ${project}/scripts/export_billing-$label.sh
        sed '/^$/N;/^\n$/D' ${project}/scripts/export_billing-$label.sh > ${tmpfile}
        mv ${tmpfile} ${project}/scripts/export_billing-$label.sh
    done
    chmod +x ${project}/scripts/*.sh

    echo ""
done

# # Build tarball for projects
# tar --exclude=**/results/* --exclude=**/exports/* --exclude=**/scripts/* -cvf projects.tar $@

# # Build packages
# echo "Build packages..."
# ./scripts/build_packages.sh

# # Update docker image
# echo "Update docker image..."
# docker build --tag microgrid-experiments:latest .

# echo ""
